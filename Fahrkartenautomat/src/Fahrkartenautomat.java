﻿import java.util.Scanner;

public class Fahrkartenautomat {
	public static void main(String[] args) {

		while(1<2) {
		double zuZahlen = fahrkartenbestellungErfassen();
		double rückgabeBetrag = fahrkartenBezahlen(zuZahlen);
		fahrkartenAusgeben();


		
		}
	}

	public static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);

		int zuZahlenderBetrag;
		int anzahlTickets;
		double gesamtPreis;

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		System.out.println("	Einzelfahrschein Regeltarif AB	[2,90 EUR] (1)");
		System.out.println("	Tageskarte Regeltarif AB		[8,60 EUR] (2)");
		System.out.println("	Kleingruppen-Tageskarte Regeltarif AB  [23,50 EUR] (3)");

		System.out.print("Ihre Wahl: ");
		zuZahlenderBetrag = tastatur.nextInt();
		while (zuZahlenderBetrag != 1 ||zuZahlenderBetrag != 2 ||zuZahlenderBetrag != 3) {
			for (int i = 0; i < 1; i++) {
				if (zuZahlenderBetrag > 3) {
					System.out.println(">> falsche Eingabe <<");
					System.out.print("Ihre Wahl: ");
					zuZahlenderBetrag = tastatur.nextInt();
				}

				if (zuZahlenderBetrag == 1) {
					double einzelkarte = 2.90;
					System.out.println("Gewünschte Anzahl der Fahrkarten ");
					anzahlTickets = tastatur.nextInt();
					gesamtPreis = anzahlTickets * einzelkarte;
					return gesamtPreis;

				} else if (zuZahlenderBetrag == 2) {
					double tageskarte = 8.60;
					System.out.println("Gewünschte Anzahl der Fahrkarten ");
					anzahlTickets = tastatur.nextInt();
					gesamtPreis = anzahlTickets * tageskarte;
					return gesamtPreis;

				} else if (zuZahlenderBetrag == 2) {
					double gruppenkarte = 23.50;
					System.out.println("Gewünschte Anzahl der Fahrkarten ");
					anzahlTickets = tastatur.nextInt();
					gesamtPreis = anzahlTickets * gruppenkarte;

				}
			}
		}

		anzahlTickets = tastatur.nextInt();
		gesamtPreis = anzahlTickets * zuZahlenderBetrag;
		return gesamtPreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {

		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;
		double rückgabeBetrag;

		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro %n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}

		rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;

		return rückgabeBetrag;
	}

	public static void fahrkartenAusgeben() {

		double eingezahlterGesamtbetrag = 0.0;
		double rückgabeBetrag;
		double zuZahlenderBetrag = 0;

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rückgeldAusgeben(double rückgabeBetrag) {

		double eingezahlterGesamtbetrag;
		double zuZahlenderBetrag;

		// rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabeBetrag > 0.0) {

			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ", rückgabeBetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabeBetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabeBetrag -= 2.0;
			}
			while (rückgabeBetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabeBetrag -= 1.0;
			}
			while (rückgabeBetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabeBetrag -= 0.5;
			}
			while (rückgabeBetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabeBetrag -= 0.2;
			}
			while (rückgabeBetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabeBetrag -= 0.1;
			}
			while (rückgabeBetrag >= 0.049)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabeBetrag -= 0.05;
			}

		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}
