
public class Benutzer {

	String name;
	String eMailAdresse;
	String passwort;
	String berechtigungsstatus;
	String letztesLoginDatum;
	String anmeldestatus;

	public void setName(String neuerName) {
		this.name = neuerName;
	}

	public void setEmailAdresse(String neueEmail) {
		this.eMailAdresse = neueEmail;
	}

	public void setPasswort(String neuesPasswort) {
		this.passwort = neuesPasswort;
	}

	public void setBerechtigungsStatus(String neuerBerechtigungsStatus) {
		this.berechtigungsstatus = neuerBerechtigungsStatus;
	}

	public void setLetztesLoginDatum(String neuesDatum) {
		this.letztesLoginDatum = neuesDatum;
	}
	public void setAnmeldeStatus(String neuerAnmeldestatus) {
		this.anmeldestatus = neuerAnmeldestatus;
}
}
